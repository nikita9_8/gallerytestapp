import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Photo } from '../screens/Photo';
import { List } from '../screens/List';


const Stack = createStackNavigator();

export function Navigation() {
	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="List">
				<Stack.Screen name="List" component={List} options={{ title: 'Photo gallery' }} />
				<Stack.Screen name="Photo" component={Photo} options={{ title: 'Single photo page' }} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}