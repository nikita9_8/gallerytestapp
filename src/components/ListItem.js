import { ImageBackground, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import React from 'react';


export const ListItem = ({ item, onPressHandler }) => {
	return (
		<TouchableOpacity activeOpacity={0.7}>
			<View style={styles.listItem}>
				<TouchableWithoutFeedback onPress={() => onPressHandler(item.id)}>
					<ImageBackground source={{ uri: item.urls.thumb}} imageStyle={{ borderRadius: 25 }} style={styles.thumb} />
				</TouchableWithoutFeedback>
				<View style={styles.itemTextWrapper}>
					<Text>Author: {item.name}</Text>
					{ item.description && (<Text>Description: {item.description}</Text>) }
				</View>
			</View>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	thumb: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
		width: 50,
		height: 50,
		borderRadius: 25,
	},
	itemTextWrapper: {
		flex: 4,
		marginLeft: 10,
		justifyContent: 'center',
	},
	listItem: {
		flex: 1,
		flexDirection: 'row',
		margin: 10,
		justifyContent: 'center',
	},
});