import React from 'react';
import {
	ActivityIndicator,
	FlatList,
	StyleSheet,
} from 'react-native';
import { ListItem } from './ListItem';

export const Gallery = ({ loading, photos, onPressHandler }) => {
	return (
		<>
			{ loading ? (<ActivityIndicator size="large" style={styles.indicator} />) : (
					<FlatList
						data={photos}
						renderItem={({ item }) => (
							<ListItem item={item} onPressHandler={onPressHandler} />
						)}
						keyExtractor={item => item.id}
					/>)
			}
		</>
	)
}

const styles = StyleSheet.create({
	thumb: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
		width: 50,
		height: 50,
		borderRadius: 25,
	},
	itemTextWrapper: {
		flex: 4,
		marginLeft: 10,
		justifyContent: 'center',
	},
	listItem: {
		flex: 1,
		flexDirection: 'row',
		margin: 10,
		justifyContent: 'center',
	},
});