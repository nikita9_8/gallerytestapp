import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { galleryStore } from './reducers/gallery';

const store = combineReducers({
	gallery: galleryStore,
})

export default createStore(store, applyMiddleware(thunk));