import { LOAD_PHOTOS } from '../types';

const initialState = {
	photos: [],
	loading: true,
}

export const galleryStore = (state = initialState, action) => {
	switch (action.type) {
		case LOAD_PHOTOS:
			return {
				photos: action.payload,
				loading: false,
			};
		default: return state
	}
	return state;
}