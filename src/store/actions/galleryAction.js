import { LOAD_PHOTOS } from '../types';

export const loadingPhotos = () => async dispatch => {
	try {
		const fetchedData = await fetch('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0&per_page=20');
		const jsonData = await fetchedData.json();
		const data = jsonData.map(({ id, alt_description: description, user, urls }) =>
			({ id, description, name: user.name, urls: { thumb: urls.thumb, large: urls.large } }));

		dispatch({
			type: LOAD_PHOTOS,
			payload: data
		})
	} catch (e) {
		console.log(e);
	}
}