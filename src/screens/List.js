import React, { useEffect } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { loadingPhotos } from '../store/actions/galleryAction';
import { Gallery } from "../components/Gallery";

export const List = ({ navigation }) => {
	const photos = useSelector(state => state.gallery.photos);
	const loading = useSelector(state => state.gallery.loading);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(loadingPhotos());
	}, [dispatch]);

	const pressPhotoHandler = (id) => {
		navigation.navigate('Photo', {
			itemId: id
		})
	}

	return (
		<SafeAreaView style={styles.container}>
			<Gallery loading={loading} photos={photos} onPressHandler={pressPhotoHandler} />
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#eeeeee',
		justifyContent: 'center',
	},
});