import * as React from 'react';
import {SafeAreaView, Image, StyleSheet} from 'react-native';
import { useSelector } from 'react-redux';

export const Photo = ({ route }) => {
	const { itemId } = route.params;
	const item = useSelector(state => state.gallery.photos.find(i => i.id === itemId));
	return (
		<SafeAreaView style={styles.container}>
			<Image source={{ uri: item.urls.thumb }} style={styles.image} />
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#eeeeee',
	},
	image: {
		width: '100%',
		height: '100%',
	}
});